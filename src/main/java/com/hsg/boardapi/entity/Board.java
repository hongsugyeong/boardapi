package com.hsg.boardapi.entity;

import com.hsg.boardapi.enums.Category;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    private String name;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false)
    private LocalDate localDate;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Category category;
}
