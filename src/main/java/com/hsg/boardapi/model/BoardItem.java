package com.hsg.boardapi.model;

import com.hsg.boardapi.enums.Category;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class BoardItem {
    private Long id;
    private String name;
    private String title;
    private Category category;
}
