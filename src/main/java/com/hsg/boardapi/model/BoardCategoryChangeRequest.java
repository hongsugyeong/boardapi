package com.hsg.boardapi.model;

import com.hsg.boardapi.enums.Category;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardCategoryChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private Category category;
}
