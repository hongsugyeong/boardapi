package com.hsg.boardapi.model;

import com.hsg.boardapi.enums.Category;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardRequest {
    private String name;

    private String title;

    @Enumerated(value = EnumType.STRING)
    private Category categorys;
}
