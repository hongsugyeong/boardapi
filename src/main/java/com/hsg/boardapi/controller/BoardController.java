package com.hsg.boardapi.controller;

import com.hsg.boardapi.model.BoardCategoryChangeRequest;
import com.hsg.boardapi.model.BoardItem;
import com.hsg.boardapi.model.BoardRequest;
import com.hsg.boardapi.model.BoardResponse;
import com.hsg.boardapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/board")
public class BoardController {
    private final BoardService boardService;

    @PostMapping("/new")
    public String setBoard (@RequestBody BoardRequest request) {
        boardService.setBoard(request);

        return "OKK";
    }

    @GetMapping("/all")
    public List<BoardItem> getBoards () {
        return boardService.getBoards();
    }

    @GetMapping("/detail/{id}")
    public BoardResponse getBoard (@PathVariable long id) {
        return boardService.getBoard(id);
    }

    @PutMapping("/change/{id}")
    public String putBoard (@PathVariable long id, @RequestBody BoardCategoryChangeRequest changeCategory) {
        boardService.putBoardCategory(id, changeCategory);

        return "OKK";
    }

    @DeleteMapping("/delete/{id}")
    public String delBoard (@PathVariable long id) {
        boardService.delBoard(id);

        return "OKK";
    }
}
