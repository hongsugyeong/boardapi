package com.hsg.boardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Category {
    DAILY("일상"),
    TRAVEL("여행"),
    ISSUE("뉴스");

    private final String categoryName;
}
