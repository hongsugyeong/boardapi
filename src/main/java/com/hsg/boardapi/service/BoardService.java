package com.hsg.boardapi.service;

import com.hsg.boardapi.entity.Board;
import com.hsg.boardapi.model.BoardCategoryChangeRequest;
import com.hsg.boardapi.model.BoardItem;
import com.hsg.boardapi.model.BoardRequest;
import com.hsg.boardapi.model.BoardResponse;
import com.hsg.boardapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;

    public void setBoard (BoardRequest request) {
        Board addData = new Board();
        addData.setName(request.getName());
        addData.setTitle(request.getTitle());
        addData.setLocalDate(LocalDate.now());
        addData.setCategory(request.getCategorys());

        boardRepository.save(addData);
    }

    public List<BoardItem> getBoards () {
        List<Board> originList = boardRepository.findAll();

        List<BoardItem> result = new LinkedList<>();

        for(Board board : originList) {
            BoardItem addItem = new BoardItem();
            addItem.setId(board.getId());
            addItem.setName(board.getName());
            addItem.setTitle(board.getTitle());
            addItem.setCategory(board.getCategory());

            result.add(addItem);
        }
        return result;
    }

    public BoardResponse getBoard (long id) {
        Board board = boardRepository.findById(id).orElseThrow();

        BoardResponse addList = new BoardResponse();
        addList.setId(board.getId());
        addList.setName(board.getName());
        addList.setTitle(board.getTitle());
        addList.setCategoryName(board.getCategory().getCategoryName());
        addList.setCategory(board.getCategory());

        return addList;
    }

    public void putBoardCategory (long id, BoardCategoryChangeRequest changeCategory) {
        Board board = boardRepository.findById(id).orElseThrow();

        board.setCategory(changeCategory.getCategory());

        boardRepository.save(board);
    }

    public void delBoard (long id) {
        Board board = boardRepository.findById(id).orElseThrow();

        boardRepository.deleteById(id);
    }
}
